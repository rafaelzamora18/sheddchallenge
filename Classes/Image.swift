//
//  Image.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class Image: BaseModel {

    var height:NSNumber?
    var width:NSNumber?
    var url:String?
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "height" :"height",
            "width"  :"width",
            "url"    :"url"
        ]
    }
}
