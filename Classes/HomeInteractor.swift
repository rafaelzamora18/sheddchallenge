//
//  HomeInteractor.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

protocol HomeInteractorInput {
    func requestFeed()
}

protocol HomeInteractorOutput: class {
    func loadQuestions(questions: [Question])
}

class HomeInteractor: HomeInteractorInput {

    weak var output:HomeInteractorOutput?
    
    var currentPage:Int = 1
    let pageSize:Int = 20
    
    let soAPIClient:StackOverflowAPIClient = StackOverflowAPIClient()
    let spotifyAPIClient:SpotifyAPIClient = SpotifyAPIClient()
    var session:SpotifySession?
    
    var shouldRequestSpotifyAuthorization = true
    
    func requestFeed() {
        
        self.soAPIClient.requestQuestions(forPage: currentPage, pageSize: pageSize) { (questions, error) in
            
            if (error != nil) {
                return
            }
            
            guard let questions = questions as [Question]? else {
                return
            }
            
            let sessionGroup:DispatchGroup = DispatchGroup()
            sessionGroup.enter()
            
            sessionGroup.notify(queue: .main, execute: {
                
                let albumsGroup = DispatchGroup()
                
                albumsGroup.notify(queue: .main, execute: {
                    self.currentPage += 1
                    self.output?.loadQuestions(questions: questions)
                })
                
                if (self.session != nil) {
                    for question in questions {
                        albumsGroup.enter()
                        self.spotifyAPIClient.searchRelatedAlbumForQuestion(question: question, accessToken: self.session!.accessToken, completion: { (album, error) in
                            question.album = album
                            albumsGroup.leave()
                        })
                    }
                } else {
                    self.shouldRequestSpotifyAuthorization = false
                    albumsGroup.enter()
                    albumsGroup.leave()
                }
            })
            
            if (self.session == nil && self.shouldRequestSpotifyAuthorization) {
                self.authorizeSpotifyWithDispatchGroup(dispatchGroup: sessionGroup)
            }   else {
                sessionGroup.leave()
            }
        }
    }
    
    func authorizeSpotifyWithDispatchGroup(dispatchGroup: DispatchGroup) {
        self.spotifyAPIClient.authorizeWithCompletion { (session, error) in
            
            if (error == nil) {
                self.session = session
            }
            dispatchGroup.leave()
        }
    }
}
