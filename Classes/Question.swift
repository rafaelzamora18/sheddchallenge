//
//  Question.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class Question: BaseModel {

    var title:String = ""
    var tags:[String] = []
    var album:Album?
    var link:String?
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
                "title" :"title",
                "tags"  :"tags",
                "link"  :"link"
                ]
    }
}
