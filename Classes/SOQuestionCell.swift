//
//  SOQuestionCell.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class SOQuestionCell: BaseCell {

    @IBOutlet weak var titleLabel: BaseLabel!
    @IBOutlet weak var tagsLabel: BaseLabel!
    @IBOutlet weak var artistImageView: UIImageView!
    @IBOutlet weak var artistLabel: BaseLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.artistImageView.layer.cornerRadius = self.artistImageView.frame.size.height/2
        
        self.titleLabel.themeElement = .questionTitle
        self.tagsLabel.themeElement = .questionTags
        self.artistLabel.themeElement = .artistName
    }
    
    override class func preferredHeight() -> CGFloat {
        return 90
    }
}
