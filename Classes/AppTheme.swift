//
//  AppTheme.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

extension ThemeElement {
    static let questionTitle:ThemeElement = ThemeElement(rawValue: 0)
    static let questionTags:ThemeElement = ThemeElement(rawValue: 1)
    static let artistName:ThemeElement = ThemeElement(rawValue: 2)
    static let defaultNavigationBar:ThemeElement = ThemeElement(rawValue: 3)
}

class AppTheme: BaseTheme {
    
    static func textColorForThemeElement(element: ThemeElement) -> UIColor {
        switch element {
            
        case ThemeElement.questionTitle:
            return self.baseThemeColor()
        case ThemeElement.questionTags:
            return self.baseClearColor()
        case ThemeElement.artistName:
            return UIColor(colorLiteralRed: 0.34, green: 0.71, blue: 0.38, alpha: 1)
        case ThemeElement.defaultNavigationBar:
            return UIColor.white
            
        default:
            return UIColor.clear
        }
    }
    
    static func backgroundColorForThemeElement(element: ThemeElement) -> UIColor {
        switch element {
            
        case ThemeElement.defaultNavigationBar:
            return self.baseThemeColor()
        default:
            return UIColor.clear
        }
    }
    
    static func fontForThemeElement(element: ThemeElement) -> UIFont {
        switch element {
            
        case ThemeElement.questionTitle:
            return AppTheme.customBoldFontOfSize(size: 17)
        case ThemeElement.questionTags, ThemeElement.artistName:
            return AppTheme.customRegularFontOfSize(size: 13)
            
        default:
            return UIFont.boldSystemFont(ofSize: 18)
        }
    }
}

// MARK: - Custom functions
extension AppTheme {
    
    class func customBoldFontOfSize(size:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Medium", size: size)!
    }
    
    class func customRegularFontOfSize(size:CGFloat) -> UIFont {
        return UIFont(name: "HelveticaNeue-Light", size: size)!
    }
    
    class func baseThemeColor() -> UIColor {
        return UIColor(colorLiteralRed: 0.29, green: 0.29, blue: 0.29, alpha: 1)
    }
    
    class func baseClearColor() -> UIColor {
        return UIColor.lightGray
    }
}

