//
//  HomePresenter.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

protocol HomePresenterInput {
    func didLoadView()
    func didSelectElementAtIndex(index :Int)
    func didFinishScrolling()
}

class HomePresenter {

    weak var view:HomeViewInput?
    var interactor:HomeInteractorInput?
    
    var questions:[Question] = []
}

extension HomePresenter: HomePresenterInput {

    func didLoadView() {
        self.interactor?.requestFeed()
    }
    
    func didSelectElementAtIndex(index: Int) {
        
        let question = questions[index]
        self.view?.presentWebForQuestion(question: question)
    }
    
    func didFinishScrolling() {
        self.interactor?.requestFeed()
    }
}

extension HomePresenter: HomeInteractorOutput {
    
    func loadQuestions(questions: [Question]) {
        self.questions += questions
        self.view?.loadQuestions(questions: questions)
    }
}
