//
//  BaseLabel.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class BaseLabel: UILabel {

    var themeElement:ThemeElement? {
        set {
            guard let newValue = newValue else {
                return
            }
            
            self.textColor = AppTheme.textColorForThemeElement(element: newValue)
            self.font = AppTheme.fontForThemeElement(element: newValue)
        }
        get {
            return nil
        }
    }
}
