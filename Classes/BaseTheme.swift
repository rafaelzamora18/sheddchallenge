//
//  BaseTheme.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

struct ThemeElement: RawRepresentable, Equatable {
    var rawValue:Int
}

struct ThemeImage: RawRepresentable, Equatable {
    var rawValue:Int
}

protocol BaseTheme {
    
    static func textColorForThemeElement(element:ThemeElement) -> UIColor
    static func backgroundColorForThemeElement(element:ThemeElement) -> UIColor
    static func fontForThemeElement(element:ThemeElement) -> UIFont
}

