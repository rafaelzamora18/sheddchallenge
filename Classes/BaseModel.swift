//
//  BaseModel.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit
import Mantle

class BaseModel: MTLModel, MTLJSONSerializing {
    
    class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        assertionFailure("Override needed")
        return ["":""]
    }
}
