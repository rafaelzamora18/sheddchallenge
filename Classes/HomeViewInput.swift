//
//  HomeViewInput.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

protocol HomeViewInput: class {
    func loadQuestions(questions: [Question])
    func presentWebForQuestion(question: Question)
}
