//
//  HomeVPIFactory.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class HomeVPIFactory {

    func createHomeVPI(viewInput: HomeViewInput) -> HomePresenterInput {
        
        let presenter = HomePresenter()
        presenter.view = viewInput
        
        let interactor = HomeInteractor()
        interactor.output = presenter
        
        presenter.interactor = interactor
        
        return presenter
    }
}
