//
//  ViewFactory.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

enum ViewIdentifier {
    case home
}

class ViewFactory: NSObject {
    
    static let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
    
    class func createViewController(withIdentifier identifier: ViewIdentifier, setup: ((_ viewController: UIViewController)->())?) -> UIViewController {
        
        let storyboardIdentifier:String
        
        switch identifier {
        case .home:
            storyboardIdentifier = "Home"
            break
        }
        
        let viewController = self.mainStoryboard.instantiateViewController(withIdentifier: storyboardIdentifier)
        
        setup?(viewController)
        
        return viewController
    }
}

