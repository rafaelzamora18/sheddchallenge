//
//  StackOverflowAPIClient.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

struct StackOverflow {
    
    static let baseURL = "http://api.stackexchange.com/"
    
    struct QuestionRequest:APIRequest {
        
        static var URL = "2.2/questions"
        static var defaultParams = [
                            "order":"desc",
                            "sort" :"activity",
                            "site" :"stackoverflow"
                            ]
    }
    
}

class StackOverflowAPIClient: APIClient {

    func requestQuestions(forPage page: Int, pageSize: Int, completion: @escaping (_ questions: [Question]?, _ error:NSError?)->()) {
        
        let URL = StackOverflow.baseURL+StackOverflow.QuestionRequest.URL
        var params = StackOverflow.QuestionRequest.defaultParams
        params["page"] = "\(page)"
        params["pagesize"] = "\(pageSize)"
        
        self.requestWithMethod(method: .GET, URL: URL, parameters: params, headers: nil, modelClass: Question.self, startNode: "items") { (response, error) in
            
            completion(response as? [Question], error)
        }
    }
}
