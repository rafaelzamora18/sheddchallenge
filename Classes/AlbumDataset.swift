//
//  AlbumDataset.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit
import Mantle

class AlbumDataset: BaseModel {

    var albums:[Album] = []
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "albums"    :"items",
        ]
    }
    
    class func albumsJSONTransformer() -> ValueTransformer {
        return MTLJSONAdapter.arrayTransformer(withModelClass: Album.self)
    }
}
