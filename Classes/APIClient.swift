//
//  APIClient.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit
import Alamofire
import Mantle

enum Method {
    case POST
    case GET
    case PUT
    case PATCH
}

protocol APIClient: class {
    func requestWithMethod(method: Method, URL: String, parameters: [String:Any]?, headers: [String:String]?, modelClass: AnyClass?, startNode:String?, completion:@escaping (_ response: AnyObject?,_ error: NSError?) -> ())
}

protocol APIRequest {
    static var URL:String { get }
    static var defaultParams:[String:String] { get }
}

extension APIClient {

    /// Starts Alamofire request.
    ///
    /// - parameter method:     HTTP method of the request
    /// - parameter URL:        The URL of the service
    /// - parameter parameters: The request parameters
    /// - parameter headers: The request headers
    /// - parameter modelClass: The resulting class
    /// - parameter startNode:  The node to start the parsing
    /// - parameter completion: Completion block executed after a response from the server is returned.
    func requestWithMethod(method: Method, URL: String, parameters: [String:Any]?, headers: [String:String]?, modelClass: AnyClass?, startNode:String?, completion:@escaping (_ response: AnyObject?,_ error: NSError?) -> ()) {
        
        Alamofire.request(URL, method: self.alamoMethod(forMethod: method), parameters: parameters, encoding: URLEncoding.default, headers: headers).validate(statusCode: 200..<300).responseJSON { (response) in
            
            switch response.result {
            case .success:
                
                if let data = response.result.value {
                    if var serviceResponse:AnyObject = data as AnyObject? {
                        
                        if (startNode != nil) {
                            serviceResponse = (serviceResponse as! [String:AnyObject])[startNode!]!
                        }
                        
                        let model:AnyObject = self.modelForResponse(response: serviceResponse, modelClass: modelClass)
                        completion(model, nil)
                    } else {
                        completion(nil, nil)
                    }
                }
                
                break
                
            case .failure(let error as AFError):
                
                completion(nil, error as NSError?)
                break
                
            default:
                break
            }
        }
    }
    
    /// Return a model for a given response
    ///
    /// - parameter response:   The response from the service
    /// - parameter modelClass: The class to model
    ///
    /// - returns: The modeled response
    private func modelForResponse(response: AnyObject, modelClass: AnyClass?) -> AnyObject {
        
        guard let modelClass = modelClass else {
            return response
        }
        
        return self.modelOfClass(parseClass: modelClass, fromResponse: response)
    }
    
    /// Transforms a JSON response into a model structure.
    ///
    /// - parameter parseClass: The resulting class.
    /// - parameter response:   The JSON response to transform.
    ///
    /// - returns: The resulting model
    private func modelOfClass(parseClass: AnyClass, fromResponse response: AnyObject) -> AnyObject {
        
        let model:AnyObject
        
        do {
            if (response is NSArray) {
                model = try MTLJSONAdapter.models(of: parseClass, fromJSONArray: response as! [AnyObject]) as AnyObject
            } else {
                model = try MTLJSONAdapter.model(of: parseClass, fromJSONDictionary: response as! [NSObject : AnyObject]) as AnyObject
            }
        } catch {
            model = NSObject()
        }
        
        return model
    }
    
    /// Transforms custom HTTP method into Alamofire HTTP method.
    ///
    /// - parameter method: Custom HTTP method
    ///
    /// - returns: Alamofire HTTP method
    private func alamoMethod(forMethod method: Method) -> Alamofire.HTTPMethod {
        switch method {
        case .POST:
            return .post
        case .GET:
            return .get
        case .PUT:
            return .put
        case .PATCH:
            return .patch
        }
    }
}
