//
//  HomeViewController.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {

    var presenter:HomePresenterInput?
    var vpiFactory:HomeVPIFactory = HomeVPIFactory()
    var tableViewDataSource:ArrayDataSource?
    @IBOutlet weak var tableView: UITableView!
    
    var cache:NSCache<AnyObject, AnyObject> = NSCache()
    
    let homeWebSegueIdentifier = "homeWeb"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupVPI()
        self.presenter?.didLoadView()
        self.setupTableView()
    }
    
    func setupVPI() {
        self.presenter = self.vpiFactory.createHomeVPI(viewInput: self)
    }
    
    func questionCellSetupClosure() -> TableViewCellSetupClosure {
        return { (cell, question) in
            
            guard let cell = cell as? SOQuestionCell else {
                return
            }
            
            guard let question = question as? Question else {
                return
            }
            
            var tags = ""
            
            for tag in question.tags {
                tags = tags+"\(tag)"
                
                if tag != question.tags.last {
                    tags = tags+", "
                }
            }
            
            cell.titleLabel.text = question.title.cleanCharacters()
            cell.tagsLabel.text = tags.cleanCharacters()
            cell.artistLabel.text = question.album?.name.cleanCharacters()
            cell.artistImageView.image = nil
            
            if let url = question.album?.images.first?.url {
                self.downloadImageFrom(path: url) { (image, error) in
                    cell.artistImageView?.image = image
                }
            }
        }
    }
    
    func setupTableView() {
        
        self.addInfiniteScroll()
        
        self.tableViewDataSource = ArrayDataSource(cellIdentifier: SOQuestionCell.preferredIdentifier(), cellSetupClosure:self.questionCellSetupClosure())
        
        SOQuestionCell.registerInTableView(tableView: self.tableView)
        self.tableView.estimatedRowHeight = SOQuestionCell.preferredHeight()
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.dataSource = self.tableViewDataSource
        self.tableView.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == homeWebSegueIdentifier) {
            let question = sender as? Question
            (segue.destination as? WebViewController)?.webURL = question?.link
            segue.destination.title = question?.title
        }
    }
    
    func addInfiniteScroll() {
        
        self.tableView.addInfiniteScroll { (tableView) in
            self.presenter?.didFinishScrolling()
        }
        self.tableView.infiniteScrollIndicatorStyle = .gray
    }
}

extension HomeViewController: HomeViewInput {
    
    func loadQuestions(questions: [Question]) {
        self.tableViewDataSource?.append(items: questions)
        self.tableView.reloadData()
        self.tableView.finishInfiniteScroll()
    }
    
    func presentWebForQuestion(question: Question) {
        self.performSegue(withIdentifier: homeWebSegueIdentifier, sender: question)
    }
}

extension HomeViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter?.didSelectElementAtIndex(index: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.leastNormalMagnitude
    }
}

extension HomeViewController: ImageClient {
    
}
