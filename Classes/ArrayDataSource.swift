//
//  ArrayDataSource.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

typealias TableViewCellSetupClosure = (_ cell:UITableViewCell, _ item:AnyObject) -> ()

class ArrayDataSource: NSObject {
    
    var items:[AnyObject]
    let cellIdentifier:String
    let cellSetupClosure:TableViewCellSetupClosure
    
    init(cellIdentifier:String, cellSetupClosure:@escaping TableViewCellSetupClosure) {
        
        self.items = []
        self.cellIdentifier = cellIdentifier
        self.cellSetupClosure =  cellSetupClosure
    }
    
    func append(items:[AnyObject]) {
        self.items += items
    }
}

// MARK: - UITableViewDataSource functions
extension ArrayDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier: self.cellIdentifier, for: indexPath)
        
        let item:AnyObject = self.items[indexPath.row]
        self.cellSetupClosure(cell, item)
        
        return cell
    }
}
