//
//  WebViewController.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {

    @IBOutlet weak var webView:UIWebView!
    var webURL:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.webURL != nil {
            self.webView.loadRequest(URLRequest(url: URL(string: self.webURL!)!))
        }
    }

}
