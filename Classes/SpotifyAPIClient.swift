//
//  SpotifyAPIClient.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

struct SpotifyAccounts {
    
    static let baseURL = "https://accounts.spotify.com/"
    
    struct Authorize:APIRequest {
        
        static var URL = "authorize"
        static var defaultParams = [
            "client_id"         :"f5426adcbabc441bbdf2b7c01397d552",
            "response_type"     :"code",
            "redirect_uri"      :"sheddchallenge://"
        ]
    }
    
    struct RefreshToken:APIRequest {
        
        static var URL = "api/token"
        static var defaultParams = [
            "grant_type"        :"authorization_code",
            "redirect_uri"      :"sheddchallenge://",
            "client_id"         :"f5426adcbabc441bbdf2b7c01397d552",
            "client_secret"     :"a0aaed12093346b6b54fb63da7add619"
        ]
    }
}

struct Spotify {
    
    static let baseURL = "https://api.spotify.com/"
    
    struct Search:APIRequest {
        
        static var URL = "v1/search"
        static var defaultParams = [
            "type"     :"album",
            "limit"    :"1"
        ]
    }
}

typealias SpotifyAuthorizeClosure = (_ session: SpotifySession?, _ error: NSError?)->()

class SpotifyAPIClient: NSObject, APIClient {
    
    var authorizeCompletionClosure:SpotifyAuthorizeClosure?
    
    func authorizeWithCompletion(completion: @escaping SpotifyAuthorizeClosure) {
        
        self.authorizeCompletionClosure = completion
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.spotifyDidRedirect(_:)), name: NSNotification.Name(rawValue: spotifyDidRedirectNotification), object: nil)
        
        var loginURL = SpotifyAccounts.baseURL+SpotifyAccounts.Authorize.URL
        
        let keys = SpotifyAccounts.Authorize.defaultParams.keys
        
        for (index, key) in keys.enumerated() {
            
            if (key == keys.first) {
                loginURL = loginURL+"?"
            }
            
            loginURL = loginURL+"\(key)=\(SpotifyAccounts.Authorize.defaultParams[key]!)"
            
            if (index < (keys.count - 1)) {
                loginURL = loginURL+"&"
            }
        }
        
        loginURL = loginURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
        
        if let url = URL(string: loginURL) {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func refreshTokenWithQuery(query: String) {
        
        let code = query.replacingOccurrences(of: "code=", with: "")
        let URL = SpotifyAccounts.baseURL+SpotifyAccounts.RefreshToken.URL
        var params = SpotifyAccounts.RefreshToken.defaultParams
        params["code"] = code
        
        self.requestWithMethod(method: .POST, URL: URL, parameters: params, headers: nil, modelClass: SpotifySession.self, startNode: nil) { (session, error) in
            
            self.authorizeCompletionClosure?(session as? SpotifySession, error)
        }
    }
    
    func searchRelatedAlbumForQuestion(question: Question, accessToken: String, completion:@escaping (_ album: Album?, _ error:NSError?)->()) {
        
        guard let tag = question.tags.first else {
            return
        }
        
        self.searchRelatedAlbumForTag(tag: tag, accessToken: accessToken) { (album, error) in
            
            completion(album, error)
        }
    }
    
    func searchRelatedAlbumForTag(tag: String, accessToken: String, completion:@escaping (_ album: Album?, _ error:NSError?)->()) {
        
        let URL = Spotify.baseURL+Spotify.Search.URL
        var params = Spotify.Search.defaultParams
        params["q"] = tag
        
        self.requestWithMethod(method: .GET, URL: URL, parameters: params, headers: self.headerForToken(token: accessToken), modelClass: AlbumDataset.self, startNode: "albums") { (dataset, error) in
            
            guard let album = (dataset as? AlbumDataset)?.albums.first else {
                completion(nil, error)
                return
            }
            
            completion(album, error)
        }
    }
    
    func spotifyDidRedirect(_ notification: NSNotification) {
        
        guard let query = notification.userInfo?[spotifyDidRedirectNotificationParameterQuery] as? String else {
            return
        }
        
        NotificationCenter.default.removeObserver(self)
        
        self.refreshTokenWithQuery(query: query)
    }
    
    func headerForToken(token: String) -> [String:String] {
        return ["Authorization":"Bearer \(token)"]
    }
}
