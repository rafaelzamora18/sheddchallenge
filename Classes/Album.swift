//
//  Album.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit
import Mantle

class Album: BaseModel {

    var name:String = ""
    var images:[Image] = []
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "name"    :"name",
            "images"  :"images"
        ]
    }
    
    class func imagesJSONTransformer() -> ValueTransformer {
        return MTLJSONAdapter.arrayTransformer(withModelClass: Image.self)
    }
}
