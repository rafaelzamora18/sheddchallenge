//
//  BaseCell.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class BaseCell: UITableViewCell {

    class var defaultHeight:CGFloat { get { return 0 } }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    class func preferredIdentifier() -> String {
        return String(describing: self)
    }
    
    class func preferredHeight() -> CGFloat {
        return defaultHeight
    }
    
    class func registerInTableView(tableView: UITableView) {
        let nib:UINib = UINib(nibName: self.preferredIdentifier(), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: self.preferredIdentifier())
    }
}
