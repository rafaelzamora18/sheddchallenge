//
//  SpotifySession.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/18/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

class SpotifySession: BaseModel {

    var accessToken:String = ""
    var tokenType:String = ""
    var scope:String = ""
    var expiresIn:NSNumber?
    var refreshToken:String = ""
    
    override class func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]! {
        return [
            "accessToken"   :"access_token",
            "tokenType"     :"token_type",
            "scope"         :"scope",
            "expiresIn"     :"expires_in",
            "refreshToken"  :"refresh_token"
        ]
    }
}
