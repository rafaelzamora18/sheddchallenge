//
//  Constants.swift
//  SheddChallenge
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

let spotifyDidRedirectNotification:String = "spotifyDidRedirectNotification"
let spotifyDidRedirectNotificationParameterQuery:String = "spotifyDidRedirectNotificationParameterQuery"
