//
//  ImageClient.swift
//  SheddChallenge
//
//  Created by RAFAEL EDUARDO ZAMORA DIAZ on 18/09/2017.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import UIKit

protocol ImageClient: class {
    var cache:NSCache<AnyObject, AnyObject> { get set }
    func downloadImageFrom(path:String, completion: @escaping (_ image: UIImage?, _ error:NSError?)->())
}

extension ImageClient {

    func downloadImageFrom(path:String, completion: @escaping (_ image: UIImage?, _ error:NSError?)->()) {
        
        if let image = self.cache.object(forKey: path as AnyObject) {
            completion(image as? UIImage, nil)
            return
        }
        
        guard let url:URL = URL(string: path) else {
            return
        }
        
        let request:URLRequest = URLRequest(url: url)
        
        let session:URLSession = URLSession(configuration: URLSessionConfiguration.default)
        let task:URLSessionDownloadTask = session.downloadTask(with: request) { (location, response, error) in
            
            if (error != nil) {
                completion(nil, error as NSError?)
                return
            }
            
            DispatchQueue.main.async(execute: { () -> Void in
                
                let data:Data
                
                do {
                    data = try Data(contentsOf: location!)
                } catch {
                    return
                }
                
                if let image = UIImage(data: data) {
                    self.cache.setObject(image, forKey: path as AnyObject)
                    completion(image, nil)
                }
            })
        }
        
        task.resume()
    }
}
