//
//  SheddChallengeTests.swift
//  SheddChallengeTests
//
//  Created by Rafael Zamora on 9/17/17.
//  Copyright © 2017 Rafael Zamora. All rights reserved.
//

import XCTest
@testable import SheddChallenge
@testable import Alamofire
@testable import Mantle

extension UIViewController {
    
    func preloadView() {
        let _ = view
    }
}

class TestHomeVPIFactory: HomeVPIFactory {
    
    var testFactoryWasCalled = false
    var testInteractor:TestHomeInteractor?
    
    override func createHomeVPI(viewInput: HomeViewInput) -> HomePresenterInput {
        
        self.testFactoryWasCalled = true
        
        let homePresenter = HomePresenter()
        homePresenter.interactor = self.testInteractor
        
        return homePresenter
    }
}

class TestHomeInteractor: HomeInteractorInput {
    
    var testHomeInteractorWasCalled = false
    
    func requestFeed() {
        self.testHomeInteractorWasCalled = true
    }
}

class SheddChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func loadHomeVC(withVPIFactory vpiFactory:HomeVPIFactory) -> HomeViewController {
        let homeVC = ViewFactory.createViewController(withIdentifier: .home, setup: nil) as! HomeViewController
        homeVC.vpiFactory = vpiFactory
        homeVC.preloadView()
        
        return homeVC
    }
    
    func testThatHomeVPIFactoryWassCalled() {
        
        let testHomeVPIFactory = TestHomeVPIFactory()
        _ = self.loadHomeVC(withVPIFactory: testHomeVPIFactory)
        
        
        XCTAssertTrue(testHomeVPIFactory.testFactoryWasCalled)
    }
    
    func testThatHomeInteractorWassCalled() {
        
        let testHomeInteractor = TestHomeInteractor()
        let testHomeVPIFactory = TestHomeVPIFactory()
        testHomeVPIFactory.testInteractor = testHomeInteractor
        
        _ = self.loadHomeVC(withVPIFactory: testHomeVPIFactory)
        
        XCTAssertTrue(testHomeInteractor.testHomeInteractorWasCalled)
    }
    
}
